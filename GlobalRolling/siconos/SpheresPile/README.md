250 spheres stacked under gravity with rolling friction
==============================================

Authors: V. Acary (INRIA Rhône–Alpes)

Date: 16/05/2022

Software: Siconos

<div align="center">
        <img width="250%" src="./SpheresPile.png" alt="About screen" title="SpheresPile.png"</img>
</div>
