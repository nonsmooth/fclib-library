Chute of polyhedra with rolling friction
==============================================

Authors: V. Acary (INRIA Rhône–Alpes)

Date: 16/05/2022

Software: Siconos

<div align="center">
        <img width="250%" src="./Chute.png" alt="About screen" title="Chute.png"</img>
</div>
